//
//  AppDelegate.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow()
        let vc = GithubissueListViewController()
        let rootNav = UINavigationController(rootViewController: vc)
        window?.makeKeyAndVisible()
        window?.rootViewController = rootNav
        return true
    }
}

