//
//  CellModels.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import Foundation

enum CellModels {
    case githubIssue(GithubIssue)
    case bannder(Banner)
}
