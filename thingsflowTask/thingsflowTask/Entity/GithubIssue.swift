//
//  Models.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import Foundation

struct GithubIssue:Decodable {
    let number:Int
    let title:String
    let body:String?
}
