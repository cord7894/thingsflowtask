//
//  Banner.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import Foundation

struct Banner {
    let imageName:String
    let link:String
    var url:URL? {
       return URL(string: link)
    }
    
    init(imageName:String = "main_logo",
         link:String = "http://thingsflow.com/ko/home") {
        self.imageName = imageName
        self.link = link
    }
}
