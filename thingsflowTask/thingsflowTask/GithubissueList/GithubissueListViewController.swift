//
//  ViewController.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import UIKit
import SnapKit
import Then
import RxSwift

class GithubissueListViewController: UIViewController {
    let viewModel = GithubissueListViewModel()
    let tableView = UITableView()
    let disposeBag = DisposeBag()
    
    let serachBarButton:UIBarButtonItem = {
        return UIBarButtonItem(title: "Search", style: .plain, target: nil, action: nil)
    }()
    
    var datas:[CellModels] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setttingUI()
        settingLayout()
        bindViewModel()
    }
    
    private func bindViewModel() {
        
        viewModel.getissues
            .asDriver(onErrorJustReturn: ("",""))
            .map { "\($0.0)/\($0.1)"}
            .drive(navigationItem.rx.title)
            .disposed(by: disposeBag)
        
        viewModel.error
            .emit(onNext: { [weak self] error in
                self?.showSimpleAlert(message: error)
            })
            .disposed(by: disposeBag)
        
        viewModel.issues
            .asDriver(onErrorJustReturn: [])
            .drive(onNext: { [weak self] datas in
                self?.datas = datas
                self?.tableView.reloadData()
            }).disposed(by: disposeBag)
        
        serachBarButton.rx.tap
            .flatMap(showSearchAlert)
            .bind(to:viewModel.getissues)
            .disposed(by: disposeBag)
    }
    
    private func setttingUI() {
        view.backgroundColor = .white
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.rightBarButtonItem = serachBarButton
        
        tableView.do {
            $0.register(GithubIssueTableViewCell.self, forCellReuseIdentifier: String(describing: GithubIssueTableViewCell.self))
            $0.register(BannerTableViewCell.self, forCellReuseIdentifier: String(describing: BannerTableViewCell.self))
            $0.rowHeight = UITableView.automaticDimension
            $0.estimatedRowHeight = 100
            $0.delegate = self
            $0.dataSource = self
        }
    }
    
    private func settingLayout() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}


extension GithubissueListViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.datas[indexPath.row]
        switch data {
        case .githubIssue(let githubIsuue):
            return self.makeGithubIsuueCell(with: githubIsuue, from: tableView, indexPath: indexPath)
        case .bannder(let banner):
            return self.makeBannerCell(with: banner, from: tableView, indexPath:indexPath)
        }
    }
    
    private func makeGithubIsuueCell(with githubIssue:GithubIssue, from tableView:UITableView, indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: GithubIssueTableViewCell.self), for: indexPath) as! GithubIssueTableViewCell
        cell.configureCell(with: githubIssue)
        return cell
    }
    
    private func makeBannerCell(with banner:Banner, from tableView:UITableView, indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BannerTableViewCell.self), for: indexPath) as! BannerTableViewCell
        cell.imageView?.image = UIImage(named: banner.imageName)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.datas[indexPath.row]
        switch data {
        case .githubIssue(let githubIsuue):
            self.handleDidSelctGithubIssueCell(with: githubIsuue)
        case .bannder(let banner):
            self.handleDidSelctBannderCell(with:banner)
        }
    }
    
    private func handleDidSelctGithubIssueCell(with githubIssue:GithubIssue) {
        let githubIssueDetailViewController = GithubIssueDetailViewController(githubIssue: githubIssue)
        self.navigationController?.pushViewController(githubIssueDetailViewController, animated: true)
    }
    
    private func handleDidSelctBannderCell(with banner:Banner) {
        guard let bannerUrl = banner.url else {
            showSimpleAlert(message: "잘못된 주소입니다.")
            return
        }
        
        UIApplication.shared.open(bannerUrl, options: [:]) { [weak self] result in
            if result == false {
                self?.showSimpleAlert(message: "잘못된 주소입니다.")
            }
        }
    }
}
