//
//  BannerTableViewCell.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import UIKit

class BannerTableViewCell: UITableViewCell {
    let bannerImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setttingUI()
        settingLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setttingUI() {
        bannerImageView.do {
            $0.contentMode = .scaleAspectFit
        }
    }
    
    private func settingLayout() {
        addSubview(bannerImageView)
        
        bannerImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
