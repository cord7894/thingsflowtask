//
//  GithubissueListModel.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import Foundation
import RxSwift

struct GithubissueListModel {
    let githubAPI: GithubAPIType
    
    init(githubAPI:GithubAPIType = GithubAPI()) {
        self.githubAPI = githubAPI
    }
    
    func getIssues(org:String, repo:String) -> Observable<Result<[GithubIssue], GithubAPIError>> {
        return githubAPI.getIssues(org: org, repo: repo)
    }
}
