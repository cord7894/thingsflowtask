//
//  GithubIssueDetailViewController.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import UIKit

class GithubIssueDetailViewController: UIViewController {
    let textView = UITextView()
    let githubIssue:GithubIssue
    
    init(githubIssue:GithubIssue) {
        self.githubIssue = githubIssue
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        settingLayout()
        
        congifure(with: self.githubIssue)
    }
    
    private func congifure(with githubIssue:GithubIssue) {
        textView.text = githubIssue.body
    }
    
    private func settingLayout() {
        view.addSubview(textView)
        textView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
