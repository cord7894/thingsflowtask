//
//  GithubissueListViewModel.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import Foundation
import RxSwift
import RxCocoa
import RxOptional

struct GithubissueListViewModel {
    let disposeBag = DisposeBag()
    let getissues = BehaviorRelay<(String,String)>(value: ("apple", "swift"))
    let issues: Signal<[CellModels]>
    let error: Signal<String>
    
    init(model: GithubissueListModel = GithubissueListModel()) {
        let getIsseusResult = getissues
            .flatMap{ data -> Observable<Result<[GithubIssue], GithubAPIError>> in
                return model.getIssues(org: data.0, repo: data.1)
                    .catch { _ in
                        .just(.failure(GithubAPIError.error("잘못된 주소입니다.")))
                    }
            }
            .share()
        
        
        let getIsseusValue = getIsseusResult
            .map { result -> [CellModels]? in
                guard case .success(let value) = result else {
                    return nil
                }
                var cellModels = value.map { CellModels.githubIssue($0)}
                let bannerModel = CellModels.bannder(Banner())
                let insertIndex = cellModels.count >= 5 ? 4 : cellModels.count
                cellModels.insert(bannerModel, at: insertIndex)
                
                return cellModels
            }
            .filterNil()
        
        issues = getIsseusValue
            .asSignal(onErrorJustReturn: [])
        
        
        let getIsseusError = getIsseusResult
            .map { result -> String? in
                guard case .failure(let error) = result else {
                    return nil
                }
                return error.message
            }
            .filterNil()
        
        error = getIsseusError
            .asSignal(onErrorJustReturn: GithubAPIError.defaultError.message)
        
        Observable<Int>
            .interval(.seconds(300), scheduler: MainScheduler.instance)
            .map { [getissues] _ in getissues.value }
            .bind(to: getissues)
            .disposed(by: disposeBag)
            
    }
}

extension ObservableType {
    func handleError() -> Observable<Result<Element, Error>> {
        return self.map { .success($0) }
            .catch { .just(.failure($0))}
    }
}
