//
//  GithubIssueTableViewCell.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import UIKit

class GithubIssueTableViewCell: UITableViewCell {
    let numberLabel = UILabel()
    let titleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setttingUI()
        settingLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(with githubIssue: GithubIssue) {
        numberLabel.text = "\(githubIssue.number) - "
        titleLabel.text = githubIssue.title
    }
    
    private func setttingUI() {
        numberLabel.do {
            $0.setContentHuggingPriority(.required, for: .horizontal)
            $0.setContentCompressionResistancePriority(.required, for: .horizontal)
        }
    }
    
    private func settingLayout() {
        addSubview(numberLabel)
        addSubview(titleLabel)
        
        numberLabel.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.top.equalToSuperview().inset(8)
            $0.bottom.equalToSuperview().inset(8)
            $0.trailing.equalTo(titleLabel.snp.leading).inset(-4)
        }
        
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(8)
            $0.bottom.equalToSuperview().inset(8)
            $0.trailing.equalToSuperview().inset(8)
        }
    }
}
