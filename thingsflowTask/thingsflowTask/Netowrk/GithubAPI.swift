//
//  GithubAPI.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import Foundation
import RxSwift
import RxCocoa

enum GithubAPIError: Error {
    case error(String)
    case defaultError
    
    var message: String {
        switch self {
        case let .error(msg):
            return msg
        case .defaultError:
            return "잠시 후에 다시 시도해주세요."
        }
    }
}


protocol GithubAPIType {
    func getIssues(org:String, repo:String) -> Observable<Result<[GithubIssue], GithubAPIError>>
}


struct GithubAPI:GithubAPIType {
    private let session: URLSession
    private let baseURL: String = "https://api.github.com/repos"
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func getIssues(org:String, repo:String) -> Observable<Result<[GithubIssue], GithubAPIError>> {
        guard let url = makeURL(org: org, repo: repo) else {
            let error = GithubAPIError.error("잘못된 주소입니다. 다시 확인해주세요")
            return .just(.failure(error))
        }
        
        print(url.absoluteString)
        
        return session.rx.data(request: URLRequest(url: url))
            .map { data in
                do {
                    let beers = try JSONDecoder().decode([GithubIssue].self, from: data)
                    return .success(beers)
                } catch {
                    return .failure(.error("서버로부터 잘못된 데이터"))
                }
            }
    }
    
    func makeURL(org:String, repo:String) -> URL? {
        URL(string: "\(baseURL)/\(org)/\(repo)/issues")
    }
}
