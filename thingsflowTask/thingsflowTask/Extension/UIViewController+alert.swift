//
//  UIViewController+alert.swift
//  thingsflowTask
//
//  Created by rhino Q on 2021/09/18.
//

import UIKit
import RxSwift

extension UIViewController {
    func showSimpleAlert(title:String = "알림", message:String) {
        let alertController = UIAlertController(title: "알림", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler:nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showSearchAlert() -> Observable<(String,String)>{
        return Observable.create {  observer in
            
            let alertController = UIAlertController(title: "검색", message: nil, preferredStyle: .alert)
            alertController.addTextField { textField in
                textField.placeholder = "Organization Name"
            }
            alertController.addTextField { textField in
                textField.placeholder = "Repository Name"
            }
            
            let search = UIAlertAction(title: "검색", style: .default) { _ in
                let org = alertController.textFields?[0].text ?? ""
                let repo = alertController.textFields?[1].text ?? ""
                
                guard org.isEmpty == false, repo.isEmpty == false else {
                    return
                }
                
                observer.onNext((org, repo))
            }
            
            let cancel = UIAlertAction(title: "취소", style: .cancel, handler: nil)
            
            alertController.addAction(search)
            alertController.addAction(cancel)
            
            self.present(alertController, animated: true, completion: nil)

            return Disposables.create()
        }
    }
}
